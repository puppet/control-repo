## site.pp ##

# This file (./manifests/site.pp) is the main entry point
# used when an agent connects to a master and asks for an updated configuration.
# https://puppet.com/docs/puppet/latest/dirs_manifest.html
#
# Global objects like filebuckets and resource defaults should go in this file,
# as should the default node definition if you want to use it.

## Active Configurations ##

# Disable filebucket by default for all File resources:
# https://github.com/puppetlabs/docs-archive/blob/master/pe/2015.3/release_notes.markdown#filebucket-resource-no-longer-created-by-default
File { backup => false }

## Node Definitions ##

# The default node definition matches any node lacking a more specific node
# definition. If there are no other node definitions in this file, classes
# and resources declared in the default node definition will be included in
# every node's catalog.
#
# Note that node definitions in this file are merged with node data from the
# Puppet Enterprise console and External Node Classifiers (ENC's).
#
# For more on node definitions, see: https://puppet.com/docs/puppet/latest/lang_node_definitions.html
node default {
  # This is where you can declare classes for all nodes.
  # Example:
  #   class { 'my_class': }
}

node 'elk-server.openstacklocal' {
  # Configure java
  # JAVA_HOME needs to be set for various ELK components to work properly
  class { 'java' :
    package => 'openjdk-8-jre',
    java_home => '/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java',
  }

  # Load Elastic Repo. Contains various packages
  include elastic_stack::repo

  # Default Elasticsearch setup
  class { 'elasticsearch': }

  # Default Kibana setup
  class { 'kibana': }

  # Set up a reverse proxy with nginx
  # Needed in order to view the Kibana dashboard in a web browser
  class { 'nginx': }
  nginx::resource::server { 'default':
      listen_port => 80,
      proxy       => 'http://localhost:5601',
      listen_options => 'default_server',
  }

  # Configure logstash
  include logstash

  # Install and configure heartbeat
  package {'heartbeat-elastic':
    ensure => present,
  } ~> file {'/etc/heartbeat/heartbeat.yml':
    ensure => present,
    source => 'puppet:///modules/beats_conf/heartbeat.yml',
  } ~> service {'heartbeat-elastic':
    ensure => running,
    enable => true,
  }

  # Load config file from repo
  file {'/etc/filebeat/filebeat.yml':
    ensure => present,
    source => 'puppet:///modules/beats_conf/filebeat.yml',
    require => Package['filebeat'],
    notify => Service['filebeat'],
  }

  # Install filebeat
  package {'filebeat':
    ensure => present,
  }

  # Enable the filebeat nginx module
  file { '/etc/filebeat/modules.d/nginx.yml':
    ensure => link,
    target => '/etc/filebeat/modules.d/nginx.yml.disabled',
    require => Package['filebeat'],
    notify => Service['filebeat'],
  }

  # Enable the filebeat system module
  file { '/etc/filebeat/modules.d/system.yml':
    ensure => link,
    target => '/etc/filebeat/modules.d/system.yml.disabled',
    require => Package['filebeat'],
    notify => Service['filebeat'],
  }

  service {'filebeat':
    ensure => running,
    enable => true,
  }


  ### Config files for logstash

  # Make logstash use beats as an input
  logstash::configfile { 'beats':
    content => 'input { beats { port => 5044 } }',
    path => '/etc/logstash/conf.d/02-beats-input.conf',
  }

  # Use heartbeat as an input
  logstash::configfile { 'heartbeat':
    content => 'input { heartbeat {} }',
    path => '/etc/logstash/conf.d/03-heartbeat-input.conf',
  }

  # Configure output to Elasticsearch
  logstash::configfile { 'es-01-out':
    source => 'puppet:///modules/logstash_conf/30-elasticsearch-output.conf',
    path   => '/etc/logstash/conf.d/30-elasticsearch-output.conf'
  }

}

node 'app-server.openstacklocal' {
  # Set up a default apache server
  include apache

  # Load Elastic stack repo to get various packages
  include elastic_stack::repo

  # Configure filebeat
  file {'/etc/filebeat/filebeat.yml':
    ensure => present,
    source => 'puppet:///modules/beats_conf/filebeat.yml',
    require => Package['filebeat'],
    notify => Service['filebeat'],
  }

  # Install filebeat
  package {'filebeat':
    ensure => present,
  }

  # Enable the filebeat apache module
  file { '/etc/filebeat/modules.d/apache.yml':
      ensure => link,
      target => '/etc/filebeat/modules.d/apache.yml.disabled',
      require => Package['filebeat'],
      notify => Service['filebeat'],
  }

  # Enable the filebeat apache module
  file { '/etc/filebeat/modules.d/system.yml':
      ensure => link,
      target => '/etc/filebeat/modules.d/system.yml.disabled',
      require => Package['filebeat'],
      notify => Service['filebeat'],
  }

  service {'filebeat':
    ensure => running,
    enable => true,
  }
}
